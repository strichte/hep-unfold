# Histogram unfolding for high energy physics

**Currently this software only automates writing configuration files for the [EWUnfolding](https://svnweb.cern.ch/trac/atlasphys/browser/Physics/StandardModel/ElectroWeak/Analyses/EWUnfolding), serving as a handy frontend.** To actually unfold, manually call EWUnfolding on the generated config files. Hep-unfold itself does not provide unfolding functionality and does not directly call the backend yet.

## About

This is a Python package operating on [ROOT](http://root.cern.ch) histograms.

### Requirements

* Python 2.7 or later

To actually unfold:

* [EWUnfolding](https://svnweb.cern.ch/trac/atlasphys/browser/Physics/StandardModel/ElectroWeak/Analyses/EWUnfolding) and the dependencies thereof

## Usage

Simply download and add the top-level directory of the package to your `PYTHONPATH`. This can be done with `source setup.sh`.

You can now `import unfolding` in Python. Take a look at and execute `examples/tryme.py` to see the user interface in action. Typical usage of the package consists of writing Python scripts like this. They may be more compilcated, as exemplified by `examples/real-world-example.py`

For detailed instructions please see the `docs` folder.
