'''
Auxiliary functions for unfolding frontend.
'''

def set_responsenames(responsenames):
    defaults = {"fidCorrNum" : "fidcorrnum",
                "fidCorrDenom" : "fidcorrden",
                "effCorrNum" : "effcorrnum",
                "effCorrDenom" : "effcorrden",
                "purityNum" : "puritynum",
                "stabilityDenom" : "stabilityden",
                "Response" : "migration_matrix"}
    needed = defaults.keys()
    try:
        if responsenames == "default":
            return defaults
    except TypeError:
        pass # can use pass here as would have return'ed if try had been successful
    if not set(responsenames.keys()).intersection(set(needed)) == set(needed):
        raise RuntimeError("Not all needed response histogram names are provided, need: " + ", ".join(needed) + "." )
    return responsenames

def determine_up_or_down(variationname):
    '''
    Determine if a systematic variation is "Up" or "Down" based on its name
    '''
    variationname_lower = variationname.lower()
    if "down" in variationname_lower or "lo" in variationname_lower:
        return "Down"
    if "up" in variationname_lower or "hi" in variationname_lower:
        return "Up"
    raise RuntimeError("Could not determine if systematic variation with name '" + variationname + "' is 'Up' or 'Down'. Please specify manually using the 'up_or_down' keyword argument.")
