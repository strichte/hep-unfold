# Using EWUnfolding

EWUnfolding is an ATLAS package that is developed independently from hep-unfold.

The version that was tested with hep-unfold can be obtained for instance on CERN LXPlus by doing:

```bash
svn co svn+ssh://${USER}@svn.cern.ch/reps/atlasphys/Physics/StandardModel/ElectroWeak/Analyses/EWUnfolding/branches/EWUnfolding-00-00-01-branch EWUnfolding
```
