# Using hep-unfold

## Terminology & notation

The **true distribution** is $t$.

The **measured distribution** is $m$.

The **response matrix** $R$ defines the relationship between reconstructed and true distribution, $m_i = R_{ij} t_j$. Its element $R_{ij}$ is (statistically) the probability of an event in true bin $j$ being measured in bin $i$.

The **efficiency correction** corrects for the non-unity probability of an event passing the true-event selection to also pass the reconstructed-event selection. This corresponds to the imperfect reconstruction efficiency.

The **fiducial correction** corrects for the non-unity probability of an event passing the reconstructed-event selection to also pass the true-event selection. This will typically be a small correction.

The **purity** is the fraction of events in each bin that are in this bin in both the true and the measured selection.

The **migration matrix** $M$ describes migrations of events between bins. Its element $M_{ij}$ describes the conditional probability of an event in true bin $j$ being measured in bin $i$, *given that* the event passed both the true-event selection (e.g. being in the fiducial phase space) and reconstructed-event selection (e.g. being successfully reconstructed). So unlike the response matrix, it does not include efficiency and fiducial corrections.

## Input requirements

All hep-unfold input data is in the form of **histograms**. Together with the configuration provided by the user, these define the unfolded results.

The reconstructed and fiducial distribution must have the **same binning**.

The following histograms are required.

Observable:

* **Reconstructed distribution** of the target observable for **data** as well as each **signal** and **background** prediction, filled for events passing reconstructed-event selection at reconstructed value with reconstructed-event weight.
* **Simulated true distribution** of the target observable for **signal** prediction, filled for events passing true-event selection at true value with true-event weight.

Meta-information for the unfolding, all required for **signal** prediction:

* **Fiducial correction numerator**, filled for events passing both true-event and reconstructed-event selection at reconstructed value with reconstructed-event weight.
* **Fiducial correction denominator**, filled for events passing reconstructed-event selection at reconstructed value with reconstructed-event weight.
* **Efficiency correction numerator**, filled for events passing both true-event and reconstructed-event selection at true value with reconstructed-event weight.
* **Efficiency correction denominator**, filled for events passing true-event selection at true value with true-event weight.
* **Purity numerator**, filled for events passing both true-event and reconstructed-event selection and whose true and resonstructed value lie in the same bin, at reconstructed value with reconstructed-event weight.
* **Stability denominator**, like fiducial correction denominator, but filled at true value (still with reconstructed-event weight).
* **Migration matrix**, a *two-dimensional* histogram filled for events passing both true-event and reconstructed-event selection at reconstructed value (on "x-axis") vs. true value (on "y-axis") with reconstructed-event weight.

The purity denominator is identical to the fiducial correction denominator and does not need to be given separately.

### Systematic variations

All of the above meta-info histograms as well as predicted distributions (signal & background) should be given for each systematic variation that is considered.

### About the histogram design choice

Why histogram input, rather than n-tuples, which some systems in particle physics use? Because n-tuple size scales linearly with the number of analysed events and their distributions of interest are not human-readable. So histograms seemed like the better choice.


## Configuration

### Locating input histograms

To read histogram `foo/bar` in file `dir/file.root`, set the path like this:

```
dir/file.root:foo/bar
```

i.e. separate the file path and histogram path with a colon.

